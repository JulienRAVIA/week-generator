<?php

require "vendor/autoload.php";

$year = (isset($_GET['year']) && is_numeric($_GET['year']) && in_array($_GET['year'], range(1900, 2030)))
    ? (int) $_GET['year']
    : (int) date('Y')
;
$weeks = \Xylis\WeekGenerator\Generator::generate($year, 'wednesday', 'monday');

