<?php

use Xylis\WeekGenerator\{ Generator, Collection };
use PHPUnit\Framework\TestCase;

class GeneratorTest extends TestCase
{
    /** @var Collection */
    private $weeks;

    /** @var int */
    private $year;

    public function setUp(): void
    {
        $this->year = $this->getRandomYear(2000, 2030);
        $this->weeks = Xylis\WeekGenerator\Generator::generate($this->year);
    }

    /**
     * @test
     */
    public function weeks_count()
    {
        $this->assertEquals($this->weeks->getWeeksCount(), $this->weeks->getLastWeek()->getId());
        $this->showExpected($this->weeks->getWeeksCount(), $this->weeks->getLastWeek()->getId());
    }

    /**
     * @test
     */
    public function get_week_assert()
    {
        $rand = random_int(1, 50);
        $this->assertEquals($rand, $this->weeks->getWeek($rand)->getId());
        $this->showExpected($rand, $this->weeks->getWeek($rand)->getId());
    }

    /**
     * @test
     */
    public function last_week_year_equals_to_year()
    {
        $this->assertEquals($this->year, $this->weeks->getLastWeek()->getYear());
        $this->showExpected($this->year, $this->weeks->getLastWeek()->getYear());
    }

    /**
     * @test
     */
    public function last_week_is_in_december()
    {
        $this->assertEquals(
            12,
            $this->weeks->getLastWeek()->getMonth()
        );
        $this->showExpected(12, $this->weeks->getLastWeek()->getMonth());
    }

    /**
     * @test
     * @expectedException \Xylis\WeekGenerator\Exception\DayProvidedIsInvalidException
     */
    public function day_provided_is_invalid()
    {
        Generator::generate($this->getRandomYear(2000, 2030), 'blabla', 'tuesday');
    }

    /**
     * @param $expected
     * @param $actual
     */
    protected function showExpected($expected, $actual)
    {
        echo 'Expected : ' . $expected . ' / Actual : ' . $actual . "\n";
    }

    /**
     * @param int $min
     * @param int $max
     * @return array
     */
    protected function years(int $min, int $max): array
    {
        return range($min, $max);
    }

    /**
     * @param int $min
     * @param int $max
     * @return int
     * @throws Exception
     */
    protected function getRandomYear(int $min, int $max): int
    {
        return random_int($min, $max);
    }
}
