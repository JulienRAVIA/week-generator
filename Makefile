test:
	.\vendor\bin\phpunit tests/
	.\vendor\bin\phpcs --report=full --standard=PSR2 src/
	.\vendor\bin\phpstan analyse

fix:
	.\vendor\bin\phpcbf --report=full --standard=PSR2 src/

install:
	composer install --no-dev

install_dev:
	composer install
