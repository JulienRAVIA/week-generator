<?php

namespace Xylis\WeekGenerator;

/**
 * @package Xylis\WeekGenerator
 */
class Collection
{
    /** @var array */
    protected $weeks = [];

    /** @var int */
    protected $year;

    /**
     * @param int $year
     */
    public function __construct(int $year)
    {
        $this->year = $year;
    }

    /**
     * @param Week $week
     */
    public function add(Week $week)
    {
        $week->setId(
            $this->getWeeksCount() + 1
        );
        $this->weeks[] = $week;
    }

    /**
     * @return Week[]
     */
    public function getWeeks(): array
    {
        return $this->weeks;
    }

    /**
     * @return Week|null
     */
    public function getFirstWeek(): ?Week
    {
        return $this->getWeek(1);
    }

    /**
     * @return Week|null
     */
    public function getLastWeek(): ?Week
    {
        return $this->getWeek(
            $this->getWeeksCount()
        );
    }

    /**
     * @param int $number
     * @return Week|null
     */
    public function getWeek(int $number): ?Week
    {
        $key = ($number > 0 && $number <= $this->getWeeksCount()) ? $number - 1 : null;

        return (array_key_exists($key, $this->weeks) & $key !== null) ? $this->weeks[$key] : null;
    }

    /**
     * @return int
     */
    public function getWeeksCount(): int
    {
        return count($this->weeks);
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }
}