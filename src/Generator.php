<?php

namespace Xylis\WeekGenerator;

use Carbon\Carbon;
use Carbon\CarbonInterface;
use Xylis\WeekGenerator\Exception\DayProvidedIsInvalidException;

/**
 * @package Xylis\WeekGenerator
 */
class Generator
{
    public const MONDAY = 'monday';
    public const TUESDAY = 'tuesday';
    public const WEDNESDAY = 'wednesday';
    public const THURSDAY = 'thursday';
    public const FRIDAY = 'friday';
    public const SATURDAY = 'saturday';
    public const SUNDAY = 'sunday';

    /** @var array */
    protected $weeks;

    public function __construct()
    {
        $this->weeks = [];
    }

    /**
     * @param int $year
     * @return Collection
     * @throws DayProvidedIsInvalidException
     */
    public static function generate(int $year, string $firstDay = 'monday', string $lastDay = 'sunday'): Collection
    {
        if (!in_array(strtolower($firstDay), self::getDays(), true) ||
            !in_array(strtolower($lastDay), self::getDays(), true)
        ) {
            throw new DayProvidedIsInvalidException();
        }

        $date = Carbon::create($year, 1, 1);
        $end = Carbon::create($year + 1, 1, 8)->year;

        $weeks = $date->weeksUntil($end, 1);
        $collection = new Collection($year);
        foreach ($weeks as $week) {
            $week = self::generateWeek($week, $firstDay, $lastDay);
            if($week->getYear() !== $collection->getYear()) {
                continue;
            }

            $collection->add($week);
        }

        return $collection;
    }

    /**
     * @param int $year
     * @param string $firstDay
     * @param string $lastDay
     * @return Collection
     * @throws DayProvidedIsInvalidException
     */
    public function gen(int $year, string $firstDay = 'monday', string $lastDay = 'sunday'): Collection
    {
        $weeks = self::generate($year, $firstDay, $lastDay);

        $this->weeks[] = $weeks;

        return $weeks;
    }

    /**
     * @return array
     */
    public function getGeneratedWeeks(): array
    {
        return $this->weeks;
    }

    /**
     * @param CarbonInterface $period
     *
     * @return Week
     */
    protected static function generateWeek(CarbonInterface $period, string $firstDay, string $lastDay): Week
    {
        $firstDay = array_search(strtolower($firstDay), self::getDays());
        $lastDay = array_search(strtolower($lastDay), self::getDays());

        $weekStart = $period->startOfWeek($firstDay);
        $weekEnd = $period->copy()->endOfWeek($lastDay);

        return new Week($weekStart, $weekEnd);
    }

    /**
     * @return array
     */
    protected static function getDays()
    {
        return [
            self::SUNDAY,
            self::MONDAY,
            self::TUESDAY,
            self::WEDNESDAY,
            self::THURSDAY,
            self::FRIDAY,
            self::SATURDAY,
        ];
    }
}