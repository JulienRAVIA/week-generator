<?php

namespace Xylis\WeekGenerator\Exception;

/**
 * @package Xylis\WeekGenerator
 */
class DayProvidedIsInvalidException extends \Exception
{
    protected $message = "One of provided days in invalid";
}