<?php

namespace Xylis\WeekGenerator;

/**
 * @package Xylis\Generator
 */
class Week
{
    /** @var int|int */
    protected $id;

    /** @var \DateTimeInterface */
    protected $start;

    /** @var \DateTimeInterface */
    protected $end;

    /**
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $end
     * @param int $week
     */
    public function __construct(\DateTimeInterface $start, \DateTimeInterface $end, ?int $week = null)
    {
        $this->id = $week;
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getStart(): \DateTimeInterface
    {
        return $this->start;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getEnd(): \DateTimeInterface
    {
        return $this->end;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return (int) $this->getStart()->format('Y');
    }

    /**
     * @return int
     */
    public function getMonth(): int
    {
        return (int) $this->getStart()->format('m');
    }
}